let resultsElement = document.querySelector('.results');
let sourceElement = document.querySelector('.source');
const baseUrl = "https://api.scryfall.com/cards/search?q=name="

function extracts(input, from, to) {
    const start = input.indexOf(from) + from.length;
    const distance = input.lastIndexOf(to) - start;
    return input.substr(start, distance);
}

function indexOfNaN(input) {
    let i = 0;
    for (; input[i] >= '0' && input[i] <= '9'; i++);
    return i;
}

function cleanStars(input) {
    return input.replace(/ \*([^*]+)\*/g, '');
}

function parseContext(contextWithStars) {
    const context = cleanStars(contextWithStars); // todo: No foil, alter, language nor condition
    const number = context.substr(0, indexOfNaN(context));
    const quantity = number === "" ? 1 : parseInt(number);
    var start = number === "" ? 0 : context.indexOf(' ') + 1
    var distance = (context.includes(' (') ? context.indexOf(' (') : context.length) - start;
    const name = context.substr(start, distance);
    const edition = extracts(context, '(', ')');

    return {
        "name": name,
        "quantity": quantity,
        "edition": edition
    };
}

function getEnglishName(data, name, setId) {
    // todo: should we handle name case?
    // todo: allow to get a specific edition card
    const englishName = data.data.filter(x => x.printed_name === name)[0].name
    return englishName
}

function appendCard(englishName, quantity, edition) {
    resultsElement.value += quantity + " " + englishName + " (" + edition + ")\n"
}

function clean() {
    resultsElement.value = ""
}

function fill(value) {
    value.split('\n').forEach(context => {
        const card = parseContext(context);
        const url = baseUrl + encodeURI(card.name) + "+lang=any";

        fetch(url).then(response => response.json())
            .then(data => appendCard(getEnglishName(data, card.name, card.edition), card.quantity, card.edition))
            .catch(e => console.log("Booo"))
    });
}

function renderDeck() {
    const value = sourceElement.value.trim();
    if (value === '')
        return;

    clean();
    fill(value);
}

// const context = document.querySelector('.card').textContent;
// const card = parseContext(context);
// const url = baseUrl + encodeURI(card.name);

const locationHref = new URL(window.location.href)
if (locationHref.search) {
    const searchParams = new URLSearchParams(locationHref.search)
    sourceElement.value = searchParams.get('cards')
    renderDeck()
}

document.querySelector('.translate')
    .addEventListener('click', function () {
        renderDeck()
    }, false)

document.querySelector('h1')
    .addEventListener('click', function () {
        sourceElement.value = "2 Affres du remords (THB) 83\n4 Atris, oracle des demi-vérités (THB) 209"
    }, false)

